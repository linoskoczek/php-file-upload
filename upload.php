<?php
function is_valid_name($file) {
    return preg_match('/^([-\.\w]+)$/', $file) > 0;
}

print_r($_FILES);
if(empty($_FILES)) {
    die("No file has been uploaded.");
}
if(!isset($_FILES['userfile']) || $_FILES['userfile']['error'] != 0) {
    die("Error happened");
}
if($_FILES['userfile']['size'] > 500000) {
    die("Maximum file size is 0.5MB!");
}
// USER DEFINED VALUE! But still worth checking, 
// so that file is not saved yet and $zip->open() won't have to execute.
$pathinfo = pathinfo($_FILES['userfile']['name']); 
if(!isset($pathinfo['extension']) || $pathinfo['extension'] !== 'zip') {
    die("Only zip files allowed.");
}

$uploaddir = './temp/';
$temp_upload_name = uniqid("upload-", true);
$uploadfile = $uploaddir . $temp_upload_name . ".zip";
$extension_white_list = ['css', 'json'];

echo $uploadfile;
echo '<pre>';
if(move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
    $zip = new ZipArchive;
    if ($zip->open($uploadfile) === TRUE) {
        for ($i = 0; $i < $zip->numFiles; $i++) {
            $pathinfo = pathinfo($zip->getNameIndex($i));
            if(isset($pathinfo['extension']) && in_array($pathinfo['extension'], $extension_white_list) && is_valid_name($zip->getNameIndex($i))) {
               $zip->extractTo('./upload/', $zip->getNameIndex($i));
               echo "Extracting " . $zip->getNameIndex($i) . "<br />";
            }
        }
        $zip->close();
    }
    unlink($uploadfile);
}
print "</pre>";
?>
